# Gunpla Search Aggregator

A web application that aggregates Gunpla model kit listings from various online retailers. Built with Vue.js, Node.js, and Bootstrap, this tool helps you find the best prices for Gunpla across multiple stores.

## Features

- Search across multiple Gunpla retailers simultaneously
- View prices, shipping costs, and total costs
- See thumbnail images of products
- Direct links to product pages
- Caching system to improve performance
- Rate limiting to respect retailer websites

## Prerequisites

Before running this project, make sure you have the following installed:

- Node.js (v18 or higher)
- npm (Node Package Manager)

## Installation

1. Clone the repository:
```bash
git clone [your-repository-url]
cd gunpla-search-aggregator
```

2. Install dependencies:
```bash
npm install
```

## Running the Application

The application consists of both a frontend and backend server. To run both simultaneously:

```bash
npm run dev
```

This will start:
- Frontend server at `http://localhost:5173`
- Backend API server at `http://localhost:3000`

## Development

### Project Structure

```
gunpla-search-aggregator/
├── server/
│   └── index.js         # Backend API server
├── src/
│   ├── components/      # Vue components
│   ├── App.vue         # Main Vue application
│   └── main.ts         # Application entry point
└── package.json        # Project dependencies and scripts
```

### Backend API Endpoints

#### GET /api/search
- **Query Parameters:**
  - `query` (required): Search term for Gunpla models
- **Response:** JSON array of Gunpla listings
- **Example Response:**
```json
[
  {
    "title": "RG 1/144 Nu Gundam",
    "price": 45.99,
    "shippingCost": 5.99,
    "source": "USA Gundam Store",
    "imageUrl": "https://example.com/image.jpg",
    "productUrl": "https://example.com/product"
  }
]
```

### Rate Limiting

The API implements rate limiting to prevent abuse:
- 100 requests per IP address
- 15-minute window
- Cached results for 30 minutes

## Contributing

1. Fork the repository
2. Create your feature branch (`git checkout -b feature/amazing-feature`)
3. Commit your changes (`git commit -m 'Add some amazing feature'`)
4. Push to the branch (`git push origin feature/amazing-feature`)
5. Open a Pull Request

## License

This project is licensed under the MIT License - see the LICENSE file for details.

## Acknowledgments

- USA Gundam Store
- Newtype
- Other Gunpla retailers included in the search aggregation

## Disclaimer

This tool is intended for personal use only. Please respect the terms of service and robot policies of the retailers' websites. The developers are not responsible for any misuse of this tool.

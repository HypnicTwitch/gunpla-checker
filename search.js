import fs from 'fs/promises';
import axios from 'axios';
import chalk from 'chalk'; // For colored console output
import * as cheerio from 'cheerio';
import { fileURLToPath } from 'url';

async function loadUrlList() {
    try {
        const data = await fs.readFile('url-list.json', 'utf8');
        // console.log('Loaded URL list:', JSON.parse(data)); // Debugging statement
        return JSON.parse(data);
    } catch (error) {
        console.error('Error loading URL list:', error);
        throw error;
    }
}

async function fetchProductsFromUrl(searchUrl, retailer) {
    try {
        const response = await axios.get(searchUrl);
        const html = response.data;
        // Log the first 300 characters of the returned HTML so you can inspect its structure.
        // console.log(chalk.gray(`\n[${retailer}] Raw HTML snippet:`), html.substring(0, 300));

        const $ = cheerio.load(html);
        
        // This is where you will need to adjust the selectors based on the website's HTML structure
        const products = [];
        
        const searchgrid = $('#snize-search-results-grid-mode');
            console.log(chalk.yellow(`searchgrid: [${searchgrid}]`))
        
/*
        // Example selector, adjust according to the actual HTML structure of the site
        $('.product-item').each((index, element) => {
            const searchgrid = $(element).find('.snize-search-results-grid-mode').text().trim() || 'No search grid found';
            const firstresult = $(search-grid).find('li').first();
            console.log(chalk.yellow("first result: [${firstresult}]"))
            // const title = $(element).find('.product-title').text().trim() || 'No title found';
            // const price = $(element).find('.product-price').text().trim() || 'No price found';
            let link = $(element).find('a').attr('href') || '';
            
            // If the link is relative, prepend the base URL for clarity.
            if (link && link.startsWith('/')) {
              const urlObj = new URL(searchUrl);
              link = `${urlObj.origin}${link}`;
            }
      
            products.push({ title, price, link });
        });
        */

        return products;
    } catch (error) {
        console.error(chalk.red(`Error fetching products from ${retailer}:`), error.message);
        return [];
    }
}

async function generateSearchUrls(searchTerm) {
    const urls = await loadUrlList();
    const results = {};

    console.log(chalk.cyan(`\nSearching for: "${searchTerm}"`)); // Display the current search term

    for (const [retailer, baseUrl] of Object.entries(urls)) {
        console.log(chalk.bold(`Checking site: ${retailer}`)); // Display the current site being checked
        const searchUrl = `${baseUrl}${encodeURIComponent(searchTerm)}`; // Construct the search URL
        // console.log(`Search URL: ${searchUrl}`); // Display the constructed search URL
        const products = await fetchProductsFromUrl(searchUrl, retailer);
        results[retailer] = products;

        // Log the results
        if (products.length > 0) {
            console.log(chalk.green(`\nResults from ${retailer}:`));
            products.forEach(product => {
                console.log(chalk.green(`- ${product.title} | Price: ${product.price} | Link: ${product.link}`));
            });
        } else {
            console.log(chalk.red(`No matching products found on ${retailer}`));
        }
    }

    return results;
}

// Export functions for use in other files
export { loadUrlList, generateSearchUrls };

// Run if called directly
const __filename = fileURLToPath(import.meta.url);
if (__filename === process.argv[1]) {
    // Throw an error if the user did not provide a search term
    if (!process.argv[2]) {
         console.error(
            chalk.red("Error: You must provide a search term as a command line argument.")
        );
         process.exit(1);
    }
    const searchTerm = process.argv[2];
    generateSearchUrls(searchTerm)
        .then(results => {
            console.log('\nSearch Results complete.');
            console.log('-'.repeat(50));
        })
        .catch(error => {
            console.error('Error during search:', error);
        });
} 
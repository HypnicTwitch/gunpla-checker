const axios = require('axios')
/*
 * axios request in the callback form
 *
axios
    .get('https://www.reddit.com/r/programming.json')
    .then((response) => {
        console.log(response)
    })
    .catch((error) => {
        console.error(error)
    })
*/

/**
 * axios request in the form of async/await function
 */
async function getForum() {
    try {
        const response = await axios.get('https://www.reddit.com/r/programming.json')
        console.log(response)
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    getForum
}
const cheerio = require('cheerio')
const fs = require('fs')

async function simpleCheerio() {
    const simple = cheerio.load('<h2 class="title">Hello, World!</h2>')
    simple('h2.title').text('Hello, Bandit!')
    simple('h2').addClass('welcome')

    console.log(simple.html())
    //<html><head></head><body><h2 class="title welcome">Hello, Bandit!</h2></body></html>
}

async function mainCheerio() {
    const result = await cheerio.load(fs.readFileSync('./sandbox/sample-table.html'))
    result('body > table > tbody > tr > td').each((index, element) => {
        console.log(result(element).text())
    })
    result('th').each((index, element) => {
        console.log(result(element).text())
    })
}

module.exports = {
    simpleCheerio,
    mainCheerio
}
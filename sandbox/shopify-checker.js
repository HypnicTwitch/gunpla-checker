import axios from 'axios';
import chalk from 'chalk'; // For colored console output
import { fileURLToPath } from 'url';

// Filter for Shopify sites from your URL list
const shopifySites = {
    az_toy_hobby: "https://aztoyhobby.com",
    samuel_decal: "https://samueldecal.com",
    galactic_toys: "https://galactictoys.com",
    jojo_hobby: "https://www.jojohobbynstuff.com",
    robots_4_less: "https://r4lus.com",
    leaping_panda_hobbies: "https://leapingpandahobbies.com"
};

async function fetchShopifyProducts(searchTerm, site, siteName) {
    try {
        // Construct Shopify API URL
        const apiUrl = `${site}/products.json`;
        const response = await axios.get(apiUrl);
        const products = response.data.products;

        // Filter products based on search term if provided
        const filteredProducts = searchTerm 
            ? products.filter(p => p.title.toLowerCase().includes(searchTerm.toLowerCase()))
            : products;

        // Return formatted results instead of logging directly
        return {
            siteName,
            success: true,
            results: filteredProducts.map(product => ({
                title: product.title,
                price: product.variants[0].price ? `$${product.variants[0].price}` : 'N/A',
                available: product.variants[0].available ? 'Yes' : 'No',
                url: `${site}/products/${product.handle}`
            }))
        };
    } catch (error) {
        return {
            siteName,
            success: false,
            error: error.message
        };
    }
}

async function checkAllStores(searchTerm = '') {
    console.log(chalk.cyan(`Searching for: "${searchTerm || 'all products'}"`));
    
    // Process all requests in parallel but collect results
    const promises = Object.entries(shopifySites).map(([name, url]) => 
        fetchShopifyProducts(searchTerm, url, name)
    );

    const results = await Promise.all(promises);
    
    // Display results in order
    results.forEach(result => {
        console.log(chalk.blue(`\n=== ${result.siteName.toUpperCase()} ===`));
        
        if (!result.success) {
            console.log(chalk.red(`Error fetching data: ${result.error}`));
            return;
        }

        if (result.results.length === 0) {
            console.log(chalk.yellow('No matching products found'));
            return;
        }

        result.results.forEach(product => {
            console.log(chalk.green('\nProduct:'), chalk.white(product.title));
            console.log(chalk.yellow('Price:'), chalk.white(product.price));
            console.log(chalk.yellow('Available:'), chalk.white(product.available));
            console.log(chalk.yellow('URL:'), chalk.white(product.url));
        });
    });
}

// Check if file is being run directly
if (fileURLToPath(import.meta.url) === process.argv[1]) {
    const searchTerm = process.argv[2] || '';
    checkAllStores(searchTerm);
}

export { checkAllStores }; 
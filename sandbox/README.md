# Sandbox Notes

How to run these files:

First, the prereqs
1. Install Node Version Manager (NVM)
   1. ```curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash```
1. Restart your terminal or run:
   1. ```source ~/.bashrc```
1. Install the latest LTS version
   1. ```nvm install --lts```
1. Install packages
   1. ```npm install axios@latest cheerio@latest```

Run the various functions by commmenting/uncommenting the functions under sandbox.js

Run this command to run the file:
```node sandbox.js```

---

## Shopify Checker

Run this command to run the file:
```node shopify-checker.js "{search term}"```

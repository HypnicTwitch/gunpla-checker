const simpleaxios = require('./simpleaxios')
const simplecheerio = require('./simplecheerio')
const bookscraper = require('./bookscraper.js')
const gundamsearch = require('./gundamsearch.js')

// simpleaxios.getForum()
// console.log('==========================')

// simplecheerio.simpleCheerio()
// console.log('==========================')

// simplecheerio.mainCheerio()
// console.log('==========================')

// bookscraper.printNavBarLinks()


if (!gundamsearch) {
    throw new Error('Required module gundamsearch failed to load')
}

try {
    //get search term from command line
    const searchTerm = process.argv[2]

    console.log('Starting search operation')

    // If no search term provided, show usage
    if (!searchTerm) {
        console.log('Usage: node sandbox.js <search term>')
        process.exit(1)
    }

    gundamsearch.searchUSAGS(searchTerm)
} catch (error) {
    console.error('Search failed:',error.message)
}

function validateSearchTerm(term) {
  if (!term || typeof term !== 'string') {
    throw new Error('Invalid search term')
  }
  return term.trim()
}

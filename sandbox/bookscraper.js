const axios = require('axios')
const cheerio = require('cheerio')

const baseurl = 'http://books.toscrape.com'

async function printNavBarLinks() {
    //make the call to retrieve the book site's home page
    //apparently I HAVE to use the word 'data' here for axios to save the response
    const { data } = await axios.get(baseurl)

    //...then load the data into cheerio for manipulation
    //using $ to keep in line with jQuery conventions
    const $ = cheerio.load(data)

    $('.nav').each((index, element) => {
        //TODO: why is there so much whitespace around these?
        console.log($(element).text())
    })

}

module.exports = {
    printNavBarLinks
}
# gunpla-checker

## Reasoning
There are soooo many places to buy gunpla.
And all the sites are different in their costs and product availabilities. Some charge tax, some have flat rate shipping, some have sales...just a lot of variables. 
This is an attempt to build a gunpla meta-search tool. My intent is to search a lot of *reputable* gunpla shops so I can, at a glance, get an idea of which sites have what I'm looking for and a ballpark idea of the cost.

## stack
  * [Node.js](https://nodejs.org/en)
    * runtime environment
  * [Puppeteer](https://pptr.dev/) 
    * for automation
  * [Cheerio](https://github.com/cheeriojs/cheerio)
    * for HTML parsing
  * [Axios](https://github.com/axios/axios)
    * HTML client for requesting pages

## Reference material
10.26.2023 - Shopify sites spit out json for free? Nice!
https://dev.to/barbaraulowee/shopify-scraper-101-how-to-scrape-shopify-store-data-with-python-4e3o

08.21.2023 - now i'm looking at this, more recent, blog post:
https://www.scrapingbee.com/blog/web-scraping-javascript/

Based on this screen scraping tutorial for right now:
https://www.digitalocean.com/community/tutorials/how-to-scrape-a-website-using-node-js-and-puppeteer

List of online gunpla retailers: https://www.reddit.com/r/Gunpla/wiki/shopping/#wiki_online_retailers

An existing crude meta-search site: http://www.gunplasearch.se/

